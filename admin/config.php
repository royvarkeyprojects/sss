<?php
// HTTP
define('HTTP_SERVER', 'http://smartsecuritysolutions.co.in/admin/');
define('HTTP_CATALOG', 'http://smartsecuritysolutions.co.in/');

// HTTPS
define('HTTPS_SERVER', 'http://smartsecuritysolutions.co.in/admin/');
define('HTTPS_CATALOG', 'http://smartsecuritysolutions.co.in/');

// DIR
define('DIR_APPLICATION', $_ENV['OPENSHIFT_REPO_DIR'].'/admin/');
define('DIR_SYSTEM', $_ENV['OPENSHIFT_REPO_DIR'].'/system/');
define('DIR_DATABASE', $_ENV['OPENSHIFT_REPO_DIR'].'/system/database/');
define('DIR_LANGUAGE', $_ENV['OPENSHIFT_REPO_DIR'].'/admin/language/');
define('DIR_TEMPLATE', $_ENV['OPENSHIFT_REPO_DIR'].'/admin/view/template/');
define('DIR_CONFIG', $_ENV['OPENSHIFT_REPO_DIR'].'/system/config/');
define('DIR_IMAGE', $_ENV['OPENSHIFT_REPO_DIR'].'/image/');
define('DIR_CACHE', $_ENV['OPENSHIFT_REPO_DIR'].'/system/cache/');
define('DIR_DOWNLOAD', $_ENV['OPENSHIFT_REPO_DIR'].'/download/');
define('DIR_LOGS', $_ENV['OPENSHIFT_REPO_DIR'].'/system/logs/');
define('DIR_CATALOG', $_ENV['OPENSHIFT_REPO_DIR'].'/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', $_ENV['OPENSHIFT_MYSQL_DB_HOST'].':'.$_ENV['OPENSHIFT_MYSQL_DB_PORT']);
define('DB_USERNAME', $_ENV['OPENSHIFT_MYSQL_DB_USERNAME']);
define('DB_PASSWORD', $_ENV['OPENSHIFT_MYSQL_DB_PASSWORD']);
define('DB_DATABASE', 'sss');
define('DB_PREFIX', 'db_');
?>