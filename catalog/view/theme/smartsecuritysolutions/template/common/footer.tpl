<div id="footer-container">
    <div id="footer" class ="container">
        <?php if ($informations) { ?>
        <div class="column">
            <h3><?php echo $text_information; ?></h3>
            <ul>
                <?php foreach ($informations as $information) { ?>
                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>
        <div class="column">
            <h3><?php echo $text_service; ?></h3>
            <ul>
                <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
                <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
            </ul>
        </div>
        <div class="column">
            <h3><?php echo $text_extra; ?></h3>
            <ul>
                <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
                <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
                <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
                <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
            </ul>
        </div>
        <div class="column">
            <h3><?php echo $text_account; ?></h3>
            <ul>
                <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
            </ul>
        </div>
    </div>
</div>
<div id="powered-container">
    <div id="powered" class = "container"><?php echo $powered; ?></div>
    </div>
</div>
<script>
								$(function(){
									var i = (!!$("#livesearch").length ? $("#livesearch") : $("<ul id='livesearch'></ul>") ), s = $("#search [name=search]");
									function repositionLivesearch() { i.css({ top: (s.offset().top+s.outerHeight()), left:s.offset().left, width: s.outerWidth() }); }
									$(window).resize(function(){ repositionLivesearch(); });
									s.keyup(function(e){
										switch (e.keyCode) {
											case 13:
												$(".active", i).length && (window.location = $(".active a", i).attr("href"));
												return false;
											break;
											case 40:
												($(".active", i).length ? $(".active", i).removeClass("active").next().addClass("active") : $("li:first", i).addClass("active"))
												return false;
											break;
											case 38:
												($(".active", i).length ? $(".active", i).removeClass("active").prev().addClass("active") : $("li:last", i).addClass("active"))
												return false;
											break;
											default:
												var query = s.val();
												if (query.length > 2) {
													$.getJSON(
														"<?php echo HTTP_SERVER; ?>?route=product/search/livesearch&search=" + query,
														function(data) {
															i.empty();
															$.each(data, function( k, v ) { i.append("<li><a href='"+v.href+"'><img src='"+v.img+"' alt='"+v.name+"'><span>"+v.name+(v.model ? "<small>"+v.model+"</small>" : '')+"</span><em>"+(v.price ? v.price : '')+"</em></a></li>") });
															i.remove(); $("body").prepend(i); repositionLivesearch();
														}
													);
												} else {
													i.empty();
												}
										}
									}).blur(function(){ setTimeout(function(){ i.hide() },500); }).focus(function(){ repositionLivesearch(); i.show(); });
								});
							</script>
							</body></html>