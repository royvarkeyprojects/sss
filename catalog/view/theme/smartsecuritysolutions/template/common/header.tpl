<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/smartsecuritysolutions/stylesheet/stylesheet.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/smartsecuritysolutions/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/smartsecuritysolutions/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php if ($stores) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
<?php } ?>
<?php echo $google_analytics; ?>
</head>
<body>
<div id="link-wrapper" class = "wrapper">
	<div id="links" class = "container clearfix">
		<div id="contact" class = "align-left">
			<a href = "#"  class="darken align-left">
				<div id = "phonenumber"><i class = "icon icon-phone"></i><?php echo $telephone; ?></div>
			</a>
			<a href = "mailto:<?php echo $email; ?>?Subject=Smart Security Solutions: Product Query"  class="darken align-left">
				<div id = "email"><i class = "icon icon-mail"></i><?php echo $email; ?></div>
			</a>
		</div>
		<div id="link" class = "align-left">
			<?php if($skype){ ?>
			<a href = "<?php echo $skype ?>"  class="highlight align-left">
				<div id = "skype"><i class = "icon icon-skype"></i></div>
			</a>
			<?php } ?>
			<?php if($facebook){ ?>
			<a href = "<?php echo $facebook ?>"  class="highlight align-left">
				<div id = "facebook"><i class = "icon icon-facebook"></i></div>
			</a>
			<?php } ?>
			<?php if($twitter){ ?>
			<a href = "<?php echo $twitter ?>"  class="highlight align-left">
				<div id = "twitter"><i class = "icon icon-twitter"></i></div>
			</a>
			<?php } ?>
			<?php if($youtube){ ?>
			<a href = "<?php echo $youtube ?>"  class="highlight align-left">
				<div id = "youtube"><i class = "icon icon-youtube"></i></div>
			</a>
			<?php } ?>
		</div>

		<div id="welcome" class = "align-right">
			<?php if (!$logged) { ?>
			    <?php echo $text_welcome; ?>
			    <?php } else { ?>
			    <?php echo $text_logged; ?>
			<?php } ?>
	    </div>

	    <div id="currency" class = "align-right">
			<?php echo $currency; ?>
		</div>
        <div id="language" class = "align-right">
            <?php echo $language; ?>
        </div>
	</div>
</div>
<div id="header-wrapper" class = "wrapper">
	<div id="header" class = "container">
	  <?php if ($logo) { ?>
	  <div id="logo"><a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a></div>
	  <?php } ?>


      <div id="search" class = "container">
            <input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
            <div class="button-search"></div>
      </div>

      <div id="cart-box-wrapper" class = "clearfix">
          <div id="cart-box">
	        <?php echo $cart; ?>
          </div>

          <div id="account-box">
              <div id="wishlist-total">
                  <a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a>
              </div>
              <div id="account-link">
                  <a href="<?php echo $account; ?>"><?php echo $text_account; ?></a>
              </div>
           </div>
      </div>

	</div>
</div>
<div id="search-wrapper" class = "wrapper">

</div>
<div id="menu-wrapper" class = "wrapper">
	<div id="menu" class = "container">
	  <ul>
	        <li><a href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
	        <li><a href="about-us">About Us</a></li>
	        <li id = "products-menu" class = "active"><a href="#">Products</a>
	        <div>
			<?php if ($categories) { ?>
				<ul class = "clearfix">
	            <?php foreach ($categories as $category) { ?>
	                <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
				    <?php if ($category['children']) { ?>
					    <div>
					        <?php for ($i = 0; $i < count($category['children']);) { ?>
					        <ul>
					          <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
					          <?php for (; $i < $j; $i++) { ?>
					          <?php if (isset($category['children'][$i])) { ?>
					          <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
					          <?php } ?>
					          <?php } ?>
					        </ul>
					        <?php } ?>
		                </div>
	                <?php } ?>
	                </li>
	            <?php } ?>
	            </ul>
	        <?php } ?>
	      </div>
	      </li>
	      <li><a href="customer-support">Customer Support</a></li>
	      <li><a href="shipping">Shipping</a></li>
	      <li><a href="faq">FAQ</a></li>
	      <li><a href="payment-options">Payment Options</a></li>
	      <li><a href="index.php?route=information/contact">Contact Us</a></li>
	  </ul>
	</div>
</div>
<?php if ($error) { ?>

    <div class="warning"><?php echo $error ?><img src="catalog/view/theme/smartsecuritysolutions/image/close.png" alt="" class="close" /></div>

<?php } ?>
<div id="notification"></div>
